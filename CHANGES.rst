Changelog
=========

1.0a2 (unreleased)
------------------

- Fix UnicodeEncodeError in calendar portlet.
  [claytonc.sousa]


1.0a1 (unreleased)
------------------

- Initial release.
