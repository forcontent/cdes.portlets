# -*- coding: utf-8 -*-
from Acquisition import aq_inner
from cdes.portlets import _
from DateTime import DateTime
from plone.app.portlets import cache
from plone.app.portlets.portlets import base
from plone.app.portlets.portlets import calendar
from plone.memoize import ram
from plone.portlets.interfaces import IPortletDataProvider
from Products.CMFCore.utils import getToolByName
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from Products.PythonScripts.standard import url_quote_plus
from StringIO import StringIO
from time import localtime
from zope import schema
from zope.component import getMultiAdapter
from zope.formlib import form
from zope.i18nmessageid import MessageFactory
from zope.interface import implementer


PLMF = MessageFactory('plonelocales')


class ICalendarPortlet(IPortletDataProvider):
    """A portlet displaying a calendar."""

    header = schema.TextLine(
        title=_(u'Header'),
        description=_(u'The header for the portlet. Leave empty for none.'),
        default=u'Events',
        required=False,
    )


@implementer(ICalendarPortlet)
class Assignment(base.Assignment):

    header = u''
    footer = u''

    def __init__(self, header=u'', footer=u''):
        self.header = header
        self.footer = footer

    @property
    def title(self):
        return _(u'Events Calendar')


def _render_cachekey(fun, self):
    context = aq_inner(self.context)
    if not self.updated:
        self.update()

    if self.calendar.getUseSession():
        raise ram.DontCache()
    else:
        portal_state = getMultiAdapter(
            (context, self.request), name='plone_portal_state')
        key = StringIO()
        key.write(portal_state.navigation_root_url())
        key.write('\n')
        key.write(cache.get_language(context, self.request))
        key.write('\n')
        key.write(self.calendar.getFirstWeekDay())
        key.write('\n')
        key.write(self.getHeader())
        key.write('\n')

        year, month = self.getYearAndMonthToDisplay()
        key.write(year)
        key.write('\n')
        key.write(month)
        key.write('\n')

        navigation_root_path = self.calendar_path()
        start = DateTime('{0}/{1}/1'.format(year, month))
        end = DateTime('%s/%s/1 23:59:59' % self.getNextMonth(year, month)) - 1  # noqa: S001

        def add(brain):
            key.write(brain.getPath())
            key.write('\n')
            key.write(brain.modified)
            key.write('\n\n')

        catalog = getToolByName(context, 'portal_catalog')
        path = navigation_root_path
        brains = catalog(
            portal_type=self.calendar.getCalendarTypes(),
            review_state=self.calendar.getCalendarStates(),
            start={'query': end, 'range': 'max'},
            end={'query': start, 'range': 'min'},
            path=path)

        for brain in brains:
            add(brain)

        return key.getvalue()


class Renderer(calendar.Renderer):

    _template = ViewPageTemplateFile('calendar.pt')

    def update(self):
        if self.updated:
            return
        self.updated = True

        context = aq_inner(self.context)
        self.calendar = getToolByName(context, 'portal_calendar')
        self._ts = getToolByName(context, 'translation_service')
        self.url_quote_plus = url_quote_plus

        self.now = localtime()
        self.yearmonth = yearmonth = self.getYearAndMonthToDisplay()
        self.year = year = yearmonth[0]
        self.month = month = yearmonth[1]

        self.showPrevMonth = yearmonth > (self.now[0] - 1, self.now[1])
        self.showNextMonth = yearmonth < (self.now[0] + 1, self.now[1])

        self.prevMonthYear, self.prevMonthMonth = self.getPreviousMonth(year, month)
        self.nextMonthYear, self.nextMonthMonth = self.getNextMonth(year, month)

        self.monthName = PLMF(
            self._ts.month_msgid(month), default=self._ts.month_english(month))

    @ram.cache(_render_cachekey)
    def render(self):
        return self._template()

    def getHeader(self):
        """Return the header for the portlet."""
        return self.data.header

    def calendar_path(self):
        """Return path calendar portlet."""
        path = getattr(self, '__portlet_metadata__', None) and \
            self.__portlet_metadata__['key'] or \
            '/'.join(self.context.getPhysicalPath())
        return path

    def event_url(self):
        """Return url for event link in calendar portlet."""
        return self.context.restrictedTraverse(self.calendar_path()).absolute_url()

    def getEventsForCalendar(self):
        """Return events for calendar portlet."""
        context = aq_inner(self.context)
        year = self.year
        month = self.month
        path = self.calendar_path()
        weeks = self.calendar.getEventsForCalendar(month, year, path=path)
        for week in weeks:
            for day in week:
                daynumber = day['day']
                if daynumber == 0:
                    continue
                day['is_today'] = self.isToday(daynumber)
                if day['event']:
                    cur_date = DateTime(year, month, daynumber)
                    localized_date = [self._ts.ulocalized_time(cur_date, context=context, request=self.request)]
                    day['eventstring'] = '\n'.join(
                        localized_date + [u' {0}'.format(self.getEventString(e)) for e in day['eventslist']])
                    day['date_string'] = '{0}-{1}-{2}'.format(year, month, daynumber)

        return weeks


class AddForm(base.AddForm):

    form_fields = form.Fields(ICalendarPortlet)

    def create(self, data):
        return Assignment(**data)


class EditForm(base.EditForm):

    form_fields = form.Fields(ICalendarPortlet)
