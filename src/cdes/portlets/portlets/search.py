# -*- coding: utf-8 -*-
from cdes.portlets import _
from plone.app.portlets.portlets import base
from plone.portlets.interfaces import IPortletDataProvider
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from zope import schema
from zope.component import getMultiAdapter
from zope.formlib import form
from zope.interface import implementer


class ISearchPortlet(IPortletDataProvider):
    """Search portlet."""

    name = schema.TextLine(
        title=_(u'label_generic_search_title', default=u'Title'),
        description=_(
            u'help_generic_search_title',
            default=u'The title of the search portlet.'
        ),
        default=u'',
        required=False
    )

    description = schema.TextLine(
        title=_(u'label_generic_search_description', default=u'Description'),
        description=_(
            u'help_generic_search_description',
            default=u'The description of the search portlet.'
        ),
        default=u'',
        required=False
    )

    types = schema.Tuple(
        title=_(u'Content type'),
        description=_(u'List of available content types.'),
        required=True,
        value_type=schema.Choice(
            vocabulary='plone.app.vocabularies.ReallyUserFriendlyTypes')
    )


@implementer(ISearchPortlet)
class Assignment(base.Assignment):

    name = u''
    description = u''
    types = u''

    def __init__(self, name=u'', description='', types=u''):
        self.name = name
        self.description = description
        self.types = types

    @property
    def title(self):
        if self.name:
            return self.name
        return _(u'Portlet Search')


class Renderer(base.Renderer):

    render = ViewPageTemplateFile('search.pt')

    def __init__(self, context, request, view, manager, data):
        base.Renderer.__init__(self, context, request, view, manager, data)

        portal_state = getMultiAdapter(
            (context, request), name='plone_portal_state')
        self.navigation_root_url = portal_state.navigation_root_url()

    def title(self):
        return self.data.name or self.data.title

    def has_name(self):
        return self.data.name

    def description(self):
        return self.data.description

    def portal_types(self):
        return self.data.types

    def search_action(self):
        url_action = '{0}/@@search'.format(self.navigation_root_url)
        return url_action

    def search_path(self):
        """Return path calendar portlet."""
        return self.__portlet_metadata__['key']


class AddForm(base.AddForm):
    form_fields = form.Fields(ISearchPortlet)
    label = _(u'Add Search Portlet')
    description = _(u'This portlet shows a search box.')

    def create(self, data):
        return Assignment(**data)


class EditForm(base.EditForm):
    form_fields = form.Fields(ISearchPortlet)
    label = _(u'Edit Search Portlet')
    description = _(u'This portlet shows a search box.')
