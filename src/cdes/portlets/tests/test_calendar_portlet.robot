*** Settings ***

Resource  portlet.robot
Library  Remote  ${PLONE_URL}/RobotRemote

Suite Setup  Open Test Browser
Suite Teardown  Close all browsers

*** Variables ***

${portletname_sample}  Events Calendar
${title_field_id}  form.header

*** Test cases ***

Test Calendar Portlet
    Enable Autologin as  Site Administrator
    Go to Homepage

    Add Right Portlet  ${portletname_sample}
    Page Should Contain Element  ${title_field_id}
    Save Portlet
    Page Should Contain Element  xpath=//table[@class='ploneCalendar']

    Edit Right Portlet
    Page Should Contain Element  id=${title_field_id}
    Input Text  id=${title_field_id}  Events Calendar
    Save Portlet
    Page Should Contain  Events Calendar
    Page Should Contain Element  xpath=//table[@class='ploneCalendar']

    Hide Right Portlet
    Go to Homepage
    Page Should Not Contain Element  xpath=//table[@class='ploneCalendar']

    Show Right Portlet
    Go to Homepage
    Page Should Contain Element  xpath=//table[@class='ploneCalendar']

    Delete Right Portlet
    Go to Homepage
    Page Should Not Contain Element  xpath=//table[@class='ploneCalendar']
