*** Settings ***

Resource  plone/app/robotframework/keywords.robot
Resource  plone/app/robotframework/saucelabs.robot

Variables  plone/app/testing/interfaces.py

*** Variables ***

${PORT} =  55001
${ZOPE_URL} =  http://localhost:${PORT}
${PLONE_URL} =  ${ZOPE_URL}/plone
${BROWSER} =  Firefox

*** Keywords ***

Manage Portlets
    Go to   ${PLONE_URL}/@@manage-portlets

Add Right Portlet
    [arguments]  ${portlet}
    Manage Portlets

    # Pass CSRF protection
    ${csrf} =  Run keyword and return status
    ...  Page should contain  Confirming User Action
    Run keyword if  ${csrf}
    ...  Click Button  form.button.confirm

    Select from list  xpath=//div[@id="portletmanager-plone-rightcolumn"]//select  ${portlet}
    Wait Until Page Contains element  name=form.actions.save

Edit Right Portlet
    Manage Portlets
    Page Should Contain Element  css=#portletmanager-plone-rightcolumn .portletHeader>a
    Click Link  css=#portletmanager-plone-rightcolumn .portletHeader>a
    Wait Until Page Contains element  name=form.actions.save

Delete Right Portlet
    Manage Portlets
    Page Should Contain Element  css=#portletmanager-plone-rightcolumn .delete button
    Click Element  css=#portletmanager-plone-rightcolumn .delete button

Hide Right Portlet
    Manage Portlets
    Page Should Contain Element  css=#portletmanager-plone-rightcolumn .portlet-action:nth-child(1) button
    Click Element  css=#portletmanager-plone-rightcolumn .portlet-action:nth-child(1) button

Show Right Portlet
    Manage Portlets
    Page Should Contain Element  css=#portletmanager-plone-rightcolumn .portlet-action:nth-child(1) button
    Click Element  css=#portletmanager-plone-rightcolumn .portlet-action:nth-child(1) button

Save Portlet
    Click Button  Save
    Wait Until Page Contains element  css=.portlets-manager
    Go to Homepage

Cancel Portlet
    Click Button  Cancel
    Wait Until Page Contains element  css=.portlets-manager
    Go to Homepage
