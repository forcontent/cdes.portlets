*** Settings ***

Resource  portlet.robot
Library  Remote  ${PLONE_URL}/RobotRemote

Suite Setup  Open Test Browser
Suite Teardown  Close all browsers

*** Variables ***

${portletname_sample}  Generic Search Portlet
${title_field_id}  form.name
${description_field_id}  form.description
${types_field_id}  form.types

*** Test cases ***

Test Calendar Portlet
    Enable Autologin as  Site Administrator
    Go to Homepage

    Add Right Portlet  ${portletname_sample}
    Page Should Contain Element  ${title_field_id}
    Select from list  id=${types_field_id}  Event
    Save Portlet
    Page Should Contain Element  xpath=//dl[@class='portlet portletSearch portletGenericSearch']

    Edit Right Portlet
    Page Should Contain Element  id=${title_field_id}
    Input Text  id=${title_field_id}  Search Portlet
    Save Portlet
    Page Should Contain Element  xpath=//dl[@class='portlet portletSearch portletGenericSearch']
    Page Should Contain  Search Portlet

    Edit Right Portlet
    Page Should Contain Element  id=${title_field_id}
    Input Text  id=${description_field_id}  Search Description
    Save Portlet
    Page Should Contain Element  xpath=//dl[@class='portlet portletSearch portletGenericSearch']
    Page Should Contain  Search Description

    Hide Right Portlet
    Go to Homepage
    Page Should Not Contain Element  xpath=//dl[@class='portlet portletSearch portletGenericSearch']

    Show Right Portlet
    Go to Homepage
    Page Should Contain Element  xpath=//dl[@class='portlet portletSearch portletGenericSearch']

    Delete Right Portlet
    Go to Homepage
    Page Should Not Contain Element  xpath=//dl[@class='portlet portletSearch portletGenericSearch']
