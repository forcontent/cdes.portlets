# -*- coding: utf-8 -*-
from cdes.portlets.portlets import search
from cdes.portlets.testing import INTEGRATION_TESTING
from plone.app.portlets.storage import PortletAssignmentMapping
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from plone.portlets.interfaces import IPortletAssignment
from plone.portlets.interfaces import IPortletDataProvider
from plone.portlets.interfaces import IPortletManager
from plone.portlets.interfaces import IPortletRenderer
from plone.portlets.interfaces import IPortletType
from zope.component import getMultiAdapter
from zope.component import getUtility

import unittest


class PortletTest(unittest.TestCase):

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])

    def test_portlet_type_registered(self):
        name = 'cdes.portlets.search'
        portlet = getUtility(IPortletType, name=name)
        self.assertEqual(portlet.addview, name)

    def test_interfaces(self):
        portlet = search.Assignment()
        self.assertTrue(IPortletAssignment.providedBy(portlet))
        self.assertTrue(IPortletDataProvider.providedBy(portlet.data))

    def test_invoke_add_view(self):
        name = 'cdes.portlets.search'
        portlet = getUtility(IPortletType, name=name)
        path = '++contextportlets++plone.leftcolumn'
        mapping = self.portal.restrictedTraverse(path)

        for m in mapping.keys():
            del mapping[m]

        addview = mapping.restrictedTraverse('+/' + portlet.addview)
        addview.createAndAdd(data={})

        self.assertEqual(len(mapping), 1)
        self.assertTrue(isinstance(mapping.values()[0], search.Assignment))

    def test_invoke_edit_view(self):
        mapping = PortletAssignmentMapping()
        mapping['search'] = search.Assignment()
        editview = getMultiAdapter(
            (mapping['search'], self.request), name='edit')
        self.assertTrue(isinstance(editview, search.EditForm))


class RenderTest(unittest.TestCase):

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])

    def get_renderer(
            self, context=None, request=None, view=None, manager=None, assignment=None):
        context = context or self.portal
        request = request or self.request
        view = view or self.portal.restrictedTraverse('@@plone')
        manager = manager or getUtility(
            IPortletManager, name='plone.rightcolumn', context=self.portal)

        return getMultiAdapter(
            (context, request, view, manager, assignment), IPortletRenderer)

    def test_invoke_renderer_view(self):
        assignment = search.Assignment()
        renderer = self.get_renderer(assignment=assignment)
        self.assertTrue(isinstance(renderer, search.Renderer))

    def test_render(self):
        assignment = search.Assignment()
        renderer = self.get_renderer(assignment=assignment)
        renderer = renderer.__of__(self.portal)
        renderer.update()
